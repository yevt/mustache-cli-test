# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Testing a command line template engine. "Mustache" as one of the most common was chosen.

### How do I get set up? ###

1. Install git.
2. Install node.js
3. `git clone git@bitbucket.org:yevt/mustache-cli-test.git`
4. Enter `mustache-cli-test` folder
5. `npm install`
6. `sh start.sh`

### Contribution guidelines ###

Look at **articles/article1.mustache** and **partials/procedure1.mustache**. This is how partial is included into the article.
**start.sh** shows how to feed article and partial to the template engine.
**data.js** is just a stub data for the stupid mustache-cli, which expects data file as it's first argument. 
The worst thing is that all partials should be specified explicitly. Another problem – the partial name isn't path-dependent.

### Who do I talk to? ###

Alexey Baskinov exclusively
